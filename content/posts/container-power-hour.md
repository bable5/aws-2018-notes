---
title: "Container Power Hour"
date: 2018-11-26T18:03:43-06:00
author: Sean L. Mooney
---

# Containers 

[Clare Liguori](https://github.com/clareliguori)

Containers with a CI server gives you a standard way to describe your build. 
The ci server does not have to know how to build rails or Java or Go. It just has to know
how to create docker containers and the Docker file describes the build. This gives
us a standard build language.

Docker-in-Docker (container docker:dind) can be used to further abstract this concept and build test runners in a container.

### What's next?

Use containers and infrastructure as code to validate an environment in the PR phase.

Example: [claire-bot](https://github.com/clareliguori/clare-bot)

* Polls for PR's
* Stands up a test cloudformation named for the PR.
* Posts link to preview stack
* Tears everything down when the PR is merged.

# Github Actions

[Jess Frazelle](https://github.com/jessfraz)

GitActions - provide 'pipelines' based on github repo events. Push style. Relieves the need to poll github for notifications all the time.


