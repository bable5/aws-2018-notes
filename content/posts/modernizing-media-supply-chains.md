---
title: "Modernizing Media Supply Chains with AWS Serverless"
date: 2018-11-27T11:30:00-08:00
draft: true
---

## Hilary Roschke - Discovery Inc.

## Jaime Valenzuela - 20th Century Fox

* Lambda's can programmatically generate web-forms or dynamically populate
data in form drop-downs

## Take away:

Both companies use step functions to create pipelines to process media.

The serverless pieces invoke other actions like lambda's, often via an API gateway.

