---
title: "Building Alexa Skills"
date: 2018-11-26T08:00:00-08:00
draft:
---

# Building an Alexa skill.

There are two sides to an functioning Alexa skill. The first voice user interface.
This needs be built in a [Amazon Developer Accunt](https://developer.amazon.com). 
Note, this is a separate account from AWS. The second side is the AWS side 
(probably a lambda) that executes the command once the voice ui has figured out 
what the user wants.

## Slots

Slots are the variables that in the voice command. They are conceptually an enumeration of possible values.

## Commands

The alexa skill has to be given examples of each possible iteration of the voice command.

For example, if we want to ask Alexa where a room is, examples commands are:

* is {RoomName} on this floor
* Where is {RoomName}

Or suppose we have a lunch database;

* What time is {MealTime}
* When is {MealTime}
* Is it time for {Mealime}

In this example, {MealTime} is the slot that can be `Breakfast`, `Lunch` or `Dinner`

## Connecting a Skill to a Lambda

First we need to write the lambda skill. This skill needs to know how to understand
the ServiceRequest that alexa will send it after parsing the voice command. The request
will have the 'utterance' (function to execute) for the skill and the values of the
variables for the slots. Once we have a lambda that understand service requests, it is
just code and the lambda can do whatever it wants. The response must be built for Alexa
to understand how to respond to the user.

